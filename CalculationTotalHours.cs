﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Itenso.TimePeriod;

namespace Sunny_Days
{
    class CalculationTotalHours
    {
        private int numberItem;

        public CalculationTotalHours(int numberItem)
        {
            this.numberItem = numberItem;
        }

        public int NumberItem
        {
            set { numberItem = value; }
        }

        public string Calculation(Period[] periods)
        {
            int count = 0;
            int totalhours = 0;
            int diffHours = 0;
            short startHour = 0;
            short finishHour = 0;
            List<DateTime> listDT = new List<DateTime>();
            DateTime dt, prevDT;
            TimeInterval tInterval;
            DateTime Start; // Время запуска
            DateTime Stoped; //Время окончания
            TimeSpan Elapsed = new TimeSpan(); // Разница
            Start = DateTime.Now; // Старт (Записываем время)

            for (int i = 0; i < numberItem; i++)
            {
                if (CheckDates(periods[i].StartDate, periods[i].FinishDate, i, true) || periods[i].ActiveDays == null || periods[i].ActiveDays.Length == 0)
                    continue;
                TimeSpan diffDays = periods[i].FinishDate.Subtract(periods[i].StartDate);
                prevDT = new DateTime();
                //текущая дата
                dt = new DateTime(periods[i].StartDate.Year, periods[i].StartDate.Month, periods[i].StartDate.Day);
                count = 0;
                //пробежимся по всему диапазону выбранных дат
                while (count++ <= diffDays.Days)
                {
                    if (prevDT.Year != 1)
                        dt = prevDT.AddDays(1);
                    prevDT = dt;
                    //if (listDT.Contains(dt))
                    //    continue;
                    if (listDT.IndexOf(dt) != -1)
                        continue;
                    if (!ContainsActiveDay(periods[i].ActiveDays, dt))
                        continue;
                    startHour = periods[i].StartHour;
                    finishHour = periods[i].FinishHour;
                    for (int j = i + 1; j < numberItem; j++)
                    {
                        tInterval = new TimeInterval(periods[j].StartDate, periods[j].FinishDate);
                        if (tInterval.HasInside(dt) && ContainsActiveDay(periods[j].ActiveDays, dt) && !CheckDates(periods[j].StartDate, periods[j].FinishDate, j) && periods[j].ActiveDays.Length != 0)
                        {
                            startHour = startHour < periods[j].StartHour ? startHour : periods[j].StartHour;
                            finishHour = finishHour > periods[j].FinishHour ? finishHour : periods[j].FinishHour;
                            listDT.Add(dt);
                        }
                    }
                    diffHours = finishHour - startHour;
                    totalhours = totalhours + diffHours;
                    startHour = 0;
                    finishHour = 0;
                }
            }
            Stoped = DateTime.Now; // Стоп (Записываем время)
            Elapsed = Stoped.Subtract(Start); // Вычитаем из Stoped (когда код выполнился) время Start (когда код запустили на выполнение)
            //MessageBox.Show("Время выполнения основного алгоритма равно " + Convert.ToString(Elapsed.Seconds) + " секунд"); // Время выполнения в секундах
            return totalhours.ToString();
        }

        private bool ContainsActiveDay(string[] activeDays, DateTime currDate)
        {
            return activeDays.Contains<string>(currDate.DayOfWeek.ToString());
        }

        private bool CheckDates(DateTime dt1, DateTime dt2, int numBlock, bool bMessageBox = false)
        {
            if (dt1 > dt2 && bMessageBox)
                MessageBox.Show("Начальная дата в " + (numBlock + 1).ToString() + " больше конечной");
            return dt1 > dt2;
        }
    }
}
