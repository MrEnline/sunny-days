﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunny_Days
{
    class Presenter
    {
        CalculationTotalHours calcTotalHours = null;
        MainWindow mainWindow = null;

        public Presenter(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            calcTotalHours = new CalculationTotalHours(mainWindow.NumberItem);
            mainWindow.eventHandler += mainWindow_myEvent;
        }

        void mainWindow_myEvent(object sender, System.EventArgs e)
        {
            calcTotalHours.NumberItem = mainWindow.NumberItem;
            mainWindow.textTotalHours.Text = calcTotalHours.Calculation(mainWindow.Periods);
        }
    }
}
