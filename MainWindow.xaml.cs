﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Sunny_Days
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>

    public struct Period
    {
        public string[] ActiveDays { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public short StartHour { get; set; }
        public short FinishHour { get; set; }
    }

    public partial class MainWindow : Window
    {
        Presenter presenter;
        public event EventHandler eventHandler = null;
        Period[] periods;
        private int numberItems;

        public MainWindow()
        {
            InitializeComponent();
            periods = new Period[1];
            numberItems = Int32.Parse(numberPeriods.Text);
            InitItem();
            InitDate();
            presenter = new Presenter(this);
        }

        private void InitDate()
        {
            StartDate.SelectedDate = DateTime.Now;
            FinishDate.SelectedDate = DateTime.Now;
        }

        private void InitItem()
        {
            periodNumber.Items.Add(1);
            periodNumber.SelectedIndex = 0;
        }

        public int NumberItem
        {
            get{return numberItems;}
        }

        public Period[] Periods
        {
            get { return periods; }
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                OnLostFocus(sender, null);
            }
        }

        private void OnMouseLeave(object sender, MouseEventArgs e)
        {
            DatePicker dp = sender as DatePicker;
            int index=0;
            if (dp.SelectedDate == null)
                return;
            if (e.LeftButton == MouseButtonState.Released)
            {
                switch (dp.Name)
                {
                    case "StartDate":
                        periods[periodNumber.SelectedIndex].StartDate = (DateTime)dp.SelectedDate;
                        break;
                    case "FinishDate":
                        periods[periodNumber.SelectedIndex].FinishDate = (DateTime)dp.SelectedDate;
                        break;
                    default:
                        break;
                }
            }
        }

        private void OnLostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (tb == null)
                return;
            int items;

            switch (tb.Name)
            {
                case "numberPeriods":
                    if (Int32.TryParse(tb.Text, out items))
                        ChangePeriods(items);
                    break;
                case "textActiveDays":
                    periods[periodNumber.SelectedIndex].ActiveDays = tb.Text.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    break;
                case "textStartHour":
                    periods[periodNumber.SelectedIndex].StartHour = RetHour(tb.Text);
                    break;
                case "textFinishHour":
                    periods[periodNumber.SelectedIndex].FinishHour = RetHour(tb.Text);
                    break;
                default:
                    break;
            }
        }

        private void ChangePeriods(int items)
        {
            Period[] tmpPeriods;
            int lengthArr = 0;
            if (items > 0 && items != periodNumber.Items.Count)
            {
                if (items > periodNumber.Items.Count)
                {
                    for (int i = periodNumber.Items.Count; i < items; i++)
                    {
                        periodNumber.Items.Add(i + 1);
                    }
                }
                if (items < periodNumber.Items.Count)
                {
                    for (int i = periodNumber.Items.Count - 1; i >= items; i--)
                    {
                        periodNumber.Items.RemoveAt(i);
                    }
                }
                tmpPeriods = new Period[items];
                lengthArr = tmpPeriods.Length > periods.Length ? periods.Length : tmpPeriods.Length;
                Array.Copy(periods, 0, tmpPeriods, 0, lengthArr);
                periods = tmpPeriods;
                numberItems = items;
            }
        }

        private short RetHour(string hour)
        {
            short retHour;
            if (Int16.TryParse(hour, out retHour))
                return retHour;
            return 0;
        }

        private void Calculation(object sender, RoutedEventArgs e)
        {
            eventHandler.Invoke(sender, e);
        }

        private void periodNumber_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            periods[periodNumber.SelectedIndex].ActiveDays = textActiveDays.Text.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            periods[periodNumber.SelectedIndex].StartDate = StartDate.DisplayDate;
            periods[periodNumber.SelectedIndex].FinishDate = FinishDate.DisplayDate;
            periods[periodNumber.SelectedIndex].StartHour = Int16.Parse(textStartHour.Text);
            periods[periodNumber.SelectedIndex].FinishHour = Int16.Parse(textFinishHour.Text); 
        }
    }
}
